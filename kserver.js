/*
  Bootstrapped from here for base web server: http://blog.modulus.io/build-your-first-http-server-in-nodejs 
  Before your interview, write a program that runs a server that is accessible on 
  http://localhost:4000/. 

  When your server receives a request on http://localhost:4000/set?somekey=somevalue 
  it should store the passed key and value in memory. 

  When it receives a request on http://localhost:4000/get?key=somekey it 
  should return the value stored at somekey.

  [km: I don't think I like the '/get' syntax requiring 'key' - is that a constant? Would any string work?
   Would/should it support '/get?anything=somekey'? I think I'd prefer '/get?somekey' (for '/set', both values vary,
   so it seems odd to have a constant string required for '/get')]
	
*/


var http = require('http');
var url = require('url');
var fs = require('fs');


const PORT=4000; 

const db_filename = "./recurse_db.txt";

// check for exceptions
var datastore = {};


fs.readFile(db_filename, 'utf8', function (err, data) {
  if (err) throw err;
  datastore = JSON.parse(data);
});



// process requests
function handleRequest(request, response){

	// split into pathname string & query object
	var url_struct = url.parse(request.url)

	// some basic error checking - we only handle set & get (without sub-paths)
	if ((url_struct.pathname.match(/\//g) || []).length != 1)
		response.end('Only URL paths "set?key=val" and "get?key=val" are supported. [Found "' + request.url + '"]' );

	// make sure we got parameters
	// [km: I think I'd prefer to move the validation of the parameters inside the dispatch functions
	//  in future because then it can be context-sensitive (e.g. 'dump' doesn't need any)]
	if (!url_struct.query || url_struct.query === "")
		response.end('Missing key and value to set. Only URL paths "set?key=val" and "get?key=keyname" are supported.');

	// for simplicity, assuming only a single key=val is passed.
	if ((url_struct.query) && (params = url_struct.query.split("=") ))
	 {
		dispatch_func = dispatch_map[url_struct.pathname];

		if(dispatch_func)
			response_string = dispatch_func(params[0], params[1]);
		else
			response_string = "Unknown function: " + url_struct.pathname;
	}

    response.end(response_string);
}

function persist(datastore) {
	fs.writeFile(db_filename, JSON.stringify(datastore),'utf8', function (err) {
  		if (err) throw err;
 		 console.log('The "data to append" was appended to file!');
	});
}



// HT http://stackoverflow.com/a/2923089 for the idea when I was searching for better option btw 'if-else' and 'switch'

var dispatch_map = 
	{
		'/set': function(key, val) {
			datastore[key] =  val;
			persist(datastore);
			resp_str = "Set '" + key + "'' : '" + datastore[key] + "'.";
			console.log(resp_str);
			return(resp_str);

		},

		'/get': function(key, val) {

			// [km: confirm that key = 'key' (see note in top comments - plan to change this syntax]
			if (key === 'key') {

				// don't worry about null at this point.
				resp_str = (datastore[val] || "No value found for key '" + val + "'.");
				console.log(resp_str);
				return(resp_str);
			}
			else
				return "Sorry, we need that 'key' string parameter ;)";
		},	

		// added for debugging/testing purposes. Sadly, based on where validation currently
		// lives, requires "/dump/?x=y" syntax. We'll get that later.
		'/dump': function(key, val) {
			return "Datastore : " + JSON.stringify(datastore);
		}

	}

// create server object
var server = http.createServer(handleRequest);



// start server
server.listen(PORT, function(){
    // log successful startup
    console.log("Server listening on: http://localhost:%s", PORT);
});
